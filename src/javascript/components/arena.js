import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal} from './modal/winner';
import { POSITIONS } from '../../constants/controls';

export function renderArena(selectedFighters) {
  const root = document.getElementById('root');
  const arena = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);

  // todo:
  // - start the fight
  // - when fight is finished show winner
  fight(...selectedFighters)
    .then(winner => {
      showWinnerModal(winner);
    })
    .catch(error => {
      console.warn(error);
    });
}

function createArena(selectedFighters) {
  const arena = createElement({ tagName: 'div', className: 'arena___root' });
  const healthIndicators = createHealthIndicators(...selectedFighters);
  const fighters = createFighters(...selectedFighters);

  const critIndicators = createCritSignals(...selectedFighters);
  const critStrips = createRageIndicators(...selectedFighters);
  
  arena.append(healthIndicators, critStrips, fighters, critIndicators);
  return arena;
}

function createHealthIndicators(leftFighter, rightFighter) {
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter, position) {
  const { name } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` }});

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter, secondFighter) {
  const battleField = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement = createFighter(firstFighter, 'left');
  const secondFighterElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter, position) {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}

function createCritSignals() {
  const container = createElement({tagName: 'div', className: `arena___crit-signals-container`});
  const leftFighterCrit = createCritSignal(POSITIONS.LEFT);
  const rightFighterCrit = createCritSignal(POSITIONS.RIGHT);

  container.append(leftFighterCrit, rightFighterCrit);
  return container;
}

function createCritSignal(position) {
  const positionClassName = position === POSITIONS.RIGHT ? `arena___right-crit-signal` : `arena___left-crit-signal`;
  const critSignalElement = createElement({
    tagName: 'div',
    className: `${positionClassName}`,
    attributes: {id: `${position}-crit-signal`}
  });

  return critSignalElement;
}

function createRageIndicators() {
  const rageIndicators = createElement({tagName: 'div', className: 'arena___rage-indicators'});
  const leftFighterIndicator = createRageIndicator(POSITIONS.LEFT);
  const rightFighterIndicator = createRageIndicator(POSITIONS.RIGHT);

  rageIndicators.append(leftFighterIndicator, rightFighterIndicator);
  return rageIndicators;
}

function createRageIndicator(position) {
  const container = createElement({tagName: 'div', className: 'arena___rage-indicator'});
  const rageStatus = createElement({tagName: 'span', className: 'arena___rage-status'});
  const indicator = createElement({tagName: 'div', className: 'arena___fighter-rage'});
  const bar = createElement({
    tagName: 'div',
    className: 'arena___rage-bar',
    attributes: {id: `${position}-rage-indicator`}
  });

  bar.innerText = 'Critical';
  indicator.append(bar);
  container.append(rageStatus, indicator);

  return container;
}

