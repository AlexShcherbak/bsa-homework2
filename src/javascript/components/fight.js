import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over

    const healthBarFirst = document.getElementById('left-fighter-indicator');
    const healthBarSecond = document.getElementById('right-fighter-indicator');
    const rageBarFirst = document.getElementById('left-rage-indicator');
    const rageBarSecond = document.getElementById('right-rage-indicator');

    const states = {
      FirstPlayer: { health: firstFighter.health, lastCriticalHit: null, isBlock: false },
      SecondPlayer: { health: secondFighter.health, lastCriticalHit: null, isBlock: false }
    };

    const keyCriticalHitFirst = new Set();
    const keyCriticalHitSecond = new Set();

    document.addEventListener('keydown', function(event) {
      const currentAction = event.code;

      switch (currentAction) {
        case controls.PlayerOneAttack:
          if (!states.SecondPlayer.isBlock && !states.FirstPlayer.isBlock) {
            states.SecondPlayer.health -= getDamage(firstFighter, secondFighter);
            states.SecondPlayer.health = states.SecondPlayer.health < 0 ? 0 : states.SecondPlayer.health;
            healthBarSecond.style.width = (100 * states.SecondPlayer.health) / secondFighter.health + '%';
          }
          break;
        case controls.PlayerOneBlock:
          states.FirstPlayer.isBlock = true;
          break;
        case controls.PlayerTwoAttack:
          if (!states.FirstPlayer.isBlock && !states.SecondPlayer.isBlock) {
            states.FirstPlayer.health -= getDamage(secondFighter, firstFighter);
            states.FirstPlayer.health = states.FirstPlayer.health < 0 ? 0 : states.FirstPlayer.health;
            healthBarFirst.style.width = (100 * states.FirstPlayer.health) / firstFighter.health + '%';
          }
          break;
        case controls.PlayerTwoBlock:
          states.SecondPlayer.isBlock = true;

        default:
          break;
      }

      if ([...controls.PlayerOneCriticalHitCombination].some((it) => it === currentAction)) {
        keyCriticalHitFirst.add(currentAction);
      } else if ([...controls.PlayerTwoCriticalHitCombination].some((it) => it === currentAction)) {
        keyCriticalHitSecond.add(currentAction);
      }

      if (
        keyCriticalHitFirst.size === 3 && [...keyCriticalHitFirst].every((it) => controls.PlayerOneCriticalHitCombination.includes(it))) {
        if (!states.FirstPlayer.isBlock && CriticalHit(states.FirstPlayer.lastCriticalHit)) {
          states.SecondPlayer.health -= getCriticalDamage(firstFighter);
          states.SecondPlayer.health = states.SecondPlayer.health < 0 ? 0 : states.SecondPlayer.health;
          healthBarSecond.style.width = (100 * states.SecondPlayer.health) / secondFighter.health + '%';
          states.FirstPlayer.lastCriticalHit = new Date();
        }
        keyCriticalHitFirst.clear();
      } else if (
        keyCriticalHitSecond.size === 3 &&
        [...keyCriticalHitSecond].every((it) => controls.PlayerTwoCriticalHitCombination.includes(it))) {
        if (!states.SecondPlayer.isBlock && CriticalHit(states.SecondPlayer.lastCriticalHit)) {
          states.FirstPlayer.health -= getCriticalDamage(secondFighter);
          states.FirstPlayer.health = states.FirstPlayer.health < 0 ? 0 : states.FirstPlayer.health;
          healthBarFirst.style.width = (100 * states.FirstPlayer.health) / firstFighter.health + '%';
          states.SecondPlayer.lastCriticalHit = new Date();
        }
        keyCriticalHitSecond.clear();
      }

      if (states.FirstPlayer.health <= 0) {
        resolve(secondFighter);
      } else if (states.SecondPlayer.health <= 0) {
        resolve(firstFighter);
      }
    });

    document.addEventListener('keyup', function(event) {
      const currentAction = event.code;

      switch (currentAction) {
        case controls.PlayerOneBlock:
          states.FirstPlayer.isBlock = false;
          break;

        case controls.PlayerTwoBlock:
          states.SecondPlayer.isBlock = false;
          break;

        default:
          break;
      }

      if ([...keyCriticalHitFirst].some((it) => it === currentAction)) {
        keyCriticalHitFirst.delete(currentAction);
      } else if ([...keyCriticalHitSecond].some((it) => it === currentAction)) {
        keyCriticalHitSecond.delete(currentAction);
      }
    });

  });
}

/*
 * попробывает переделать на setInterval
 */
function CriticalHit(lastCriticalHit) {
  return lastCriticalHit ? Math.floor((new Date() - lastCriticalHit) / 1000) > 10 : true;
}

export function getCriticalDamage(fighter) {
  const { attack } = fighter;
  return 2 * attack;
}

export function getDamage(attacker, defender) {
  // return damage
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  return blockPower > hitPower ? 0 : hitPower - blockPower;
}

export function getHitPower(fighter) {
  // return hit power
  const {attack} = fighter;
  const criticalHitChance = Math.random() + 1;
  return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  const {defense} = fighter;
  const dodgeChance = Math.random() + 1;
  return defense * dodgeChance;
}


/* export function toggleCritSignal(show, position) {
 *   const indicator = document.getElementById(`${position}-crit-signal`);
 *   if (show) {
 *     indicator.style.visibility = 'visible';
 *   } else {
 *     indicator.style.visibility = 'hidden';
 *   }
 * }
 *
 * export function updateRageIndicator(position, width) {
 *   const rageIndicator = document.getElementById(`${position}-rage-indicator`);
 *   rageIndicator.style.width = `${width}%`;
 * }
 *
 * export const createCritPointsUpdatedHandler = (position) => (currentPoints, canCrit) => {
 *   if (currentPoints === 0) {
 *     toggleCritSignal(false, position);
 *   }
 *
 *   if (canCrit) {
 *     toggleCritSignal(true, position);
 *   }
 *
 *   updateRageIndicator(position, currentPoints * 10);
 * };
 */
