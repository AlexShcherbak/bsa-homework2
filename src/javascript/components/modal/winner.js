import {showModal} from './modal';
import {createElement} from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  // call showModal function
  const title = `${fighter.name} won!`;
  const body = createElement({tagName: 'h1', className: 'modal-body'});
  body.innerText = `Winne! Name: ${fighter.name}`;

  const result = {
    title,
    bodyElement: body,
    onClose: () => {
      window.location.reload()
    },
  };
  showModal(result);
}
