import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  // todo: show fighter info (image, name, health, etc.)

  if(fighter) {
    const {name, health, attack, defense} = fighter;
    const fighterImg = createFighterImage(fighter);
    const fighterAttribute = createElement ({
      tagName: 'div',
      className: 'fighter-values'
    });
    const fighterWrapper = createElement({
      tagName: 'div',
      className: 'fighter-wrapper'
    })

    fighterAttribute.innerHTML = `
      <h2>${name}</h2>
      <div>health - ${health}</div>
      <div>attack - ${attack}</div>
      <div>defense - ${defense}</div>
    `;

    fighterWrapper.append(fighterAttribute);
    fighterElement.append(fighterImg, fighterWrapper);

  } else {
    console.log('Select second player')
  }


  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

  return imgElement;
}
